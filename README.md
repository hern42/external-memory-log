# External memory log

A lot of stuff I need to remember during my Linux adventures (espresso: laptop, cafetiere: synology nas ds228, jukebox: a raspberry pi 4...)

All is consigned in the wiki of this repository. In the repo there'll be config files and some such...

Link to the wiki: https://gitlab.com/hern42/external-memory-log/-/wikis/External-Memory-Log-a-path-towards-*nix-enlightenment-(and-a-lot-of-obscurity-as-well)
